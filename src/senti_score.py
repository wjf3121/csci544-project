from nltk.tag.stanford import NERTagger
import nltk
import json
from pymongo import MongoClient
import copy
from pprint import pprint
import jsonrpclib
from collections import defaultdict
from stanford_parser import StanfordNLP


def find_phrase(dependency):
    phrase = {}
    # result['res']=res
    # result['poss']='NONE'
    # result['modpos']='NONE'
    # result['modres']='NONE'
    # result['neg']='NONE'
    for d in dependency:
        if d[0] == 'amod':
            if d[1] not in phrase:
                phrase[d[1]] = {}
                phrase[d[1]]['adj'] = d[2]

    for d in dependency:
        if d[0] == 'advmod':
            for p in phrase:
                if d[1] == phrase[p]['adj']:
                    phrase[p]['adv'] = d[2]
    for d in dependency:
        if d[0] == 'neg':
            for p in phrase:
                if d[1] == phrase[p]['adj']:
                    phrase[p]['negadj'] = True
                if d[1] == ['adv']:
                    phrase[p]['negadv'] = True

    return phrase


if __name__ == "__main__"	:
    #st = NERTagger('/Users/maijia/work/stanford-ner-2015-04-20/ner-model.res.gz','/Users/maijia/work/stanford-ner-2015-04-20/stanford-ner.jar')
    nlp = StanfordNLP()
    db = MongoClient().yelp
    tagged_reviews = db.tagged_restaurant_review_all.find(
        no_cursor_timeout=True)
    finalphrases = defaultdict(list)
    count = 0
    jsfile = open('finalphrases', 'w')
    for tr in tagged_reviews:
        star = int(tr['stars'])
        # s="Is Border_Grill's service good?"Can you give me some recommendation for restaurant with good service
        #s=input("Ask your question here:")
        #s="Can you give me some recommendation for food of Border_Grill?"
        for s in tr['sents']:
            #s="Can't miss stop for the not very nice Fish Sandwich in Pittsburgh."
            sr = s['raw']
            sr = nltk.sent_tokenize(sr)
            # if s=="quit":
            # 	break
            #w= nltk.word_tokenize(s)
            # print(w)
            # res_tag=st.tag(w)
            #"I've seen a bunch of glowing reviews on good ol' Yelp and as I always do when I read the praise heaped on a particular place by my Yelp brethren and sistren, I thought "I'll be the judge of that!"
            for sub_sent in sr:
                print(sub_sent)
                parse_result = nlp.parse(sub_sent)
                if 'sentences' in parse_result and 'dependencies'in parse_result['sentences'][0]:
                    phrase = find_phrase(
                        parse_result['sentences'][0]['dependencies'])
                    if len(phrase) > 0:
                        finalphrases[str(star)].append(phrase)
            # pprint(parse_result)
        # break
        # if count>10:
        # 	tagged_reviews=[]
        # 	break
        # count+=1

    print(finalphrases)
    json.dump(finalphrases, jsfile, indent=4)
