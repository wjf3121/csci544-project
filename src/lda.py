#!/usr/bin/env python3
from gensim.models.ldamodel import LdaModel
from gensim import corpora
from utility import progressbar, word_filters, word_processors
from setting import Setting


def build_dict(tagged_reviews, word_filter, word_processor=lambda x: x):
    words = []
    for review in progressbar(tagged_reviews, prefix=" Build Dict", count=tagged_reviews.count()):
        doc_words = []
        for sent in review['sents']:
            doc_words.extend([word_processor(word['word'].lower())
                              for word in sent['tokenized'] if word_filter(word['word'])])
        words.append(doc_words)
    dictionary = corpora.Dictionary(words)
    dictionary.filter_extremes(keep_n=10000)
    dictionary.compactify()
    return dictionary


def build_corpus(dictionary, tagged_reviews, word_filter, word_processor=lambda x: x):
    corpus = []
    for review in progressbar(tagged_reviews, prefix=" Build Corpus", count=tagged_reviews.count()):
        doc_words = []
        for sent in review['sents']:
            doc_words.extend([word_processor(word['word'].lower())
                              for word in sent['tokenized'] if word_filter(word['word'])])
        corpus.append(dictionary.doc2bow(doc_words))
    return corpus


def train(corpus, dictionary, num_topics):
    lda = LdaModel(corpus, num_topics=num_topics, id2word=dictionary)
    return lda


def predict(lda_model, dictionary, words, word_filter, word_processor=lambda x: x):
    processed_words = [word_processor(word['word'].lower())
                       for word in words if word_filter(word['word'])]
    words_corpus = dictionary.doc2bow(processed_words)
    topics = lda_model[words_corpus]
    return topics


def save_lda_model(lda_model, path):
    lda_model.save(path)


def save_dict(dictionary, path):
    corpora.Dictionary.save(dictionary, path)


def load_lda_model(path):
    return LdaModel.load(path)


def load_dict(path):
    return corpora.Dictionary.load(path)


def print_topics(lda_model):
    for i in range(0, lda_model.num_topics):
        print(lda_model.print_topic(i))

if __name__ == '__main__':
    '''
    from pymongo import MongoClient
    test_review_id = '7JlNyWpVPTJZRkRRGv0JWA'
    db = MongoClient().yelp
    word_filter = word_filters[Setting.LDA_WORD_FILTER]
    word_processor = word_processors[Setting.LDA_WORD_PROCESSOR]

    reviews = db.tagged_restaurant_review_all.find()
    dictionary = build_dict(reviews, word_filter, word_processor)
    reviews = db.tagged_restaurant_review_all.find()
    corpus = build_corpus(dictionary, reviews, word_filter, word_processor)
    lda_model = train(corpus, dictionary, Setting.LDA_TOPIC_NUM)
    save_dict(dictionary, Setting.LDA_DICT_PATH)
    save_lda_model(lda_model, Setting.LDA_MODEL_PATH)
    '''
    lda_model = load_lda_model('../model/lda_all.mm')
    dictionary = load_dict('../model/lda_all.dict')
    for i in range(0, lda_model.num_topics):
        print(lda_model.print_topic(i))
    '''
    test_review = db.tagged_restaurant_review.find_one(
        {'review_id': test_review_id})
    raw_review = db.restaurant_review.find_one(
        {'review_id': test_review_id})
    print(raw_review)
    for sent in test_review['sents']:
        result_topics = predict(
            lda_model, dictionary, sent['tokenized'], word_filter, word_processor)
        print(result_topics)
    '''
