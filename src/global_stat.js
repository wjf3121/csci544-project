var pos_all = db.ngram_count_all.aggregate([ 
//{
//    $match : { "value.pos" : { $gt : 1 } } 
//},
{
    $group: { 
        _id: "pos", 
        total: { 
            $sum: "$value.pos" 
        } 
    } 
}]);


var neg_all = db.ngram_count_all.aggregate([ 
//{
//    $match : { "value.neg" : { $gt : 1 } } 
//},
{ 
    $group: { 
        _id: "neg", 
        total: { 
            $sum: "$value.neg" 
        } 
    } 
}
]);


db.global_stat_all.insert(pos_all.toArray());

db.global_stat_all.insert(neg_all.toArray());

db.global_stat_all.update({_id:"pos"},{$set:{doc:db.restaurant_review.find({stars:{$gt:3}}).count()}});

db.global_stat_all.update({_id:"neg"},{$set:{doc:db.restaurant_review.find({stars:{$lt:4}}).count()}});
