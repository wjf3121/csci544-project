var mapf = function() {
    function word_process(words){
        var reg = /[^0-9a-zA-Z]+/;
        new_words = [];
        for(var i = 0; i < words.length; i++){
            new_word = words[i].word.replace(reg, "");
            if(new_word !== "")
                new_words.push(words[i].word.toLowerCase());
        }
        return new_words;
    }
    var sents = this.sents;


    if (sents === null) {
        return;
    }
    for (var i = 0; i < sents.length; i++) {
        new_sent = word_process(sents[i]);
        for(var j = 0; j < new_sent.length; j++){
            emit(new_sent[j], 1);
        }
    }
};


var reducef = function(key, values) {
    var count = 0;
    values.forEach(function(v) {
        count += v;
    });
    return count;
};


db.tagged_restaurant_review.mapReduce(
                     mapf,
                     reducef,
                     { out: "token_count" }
                   );