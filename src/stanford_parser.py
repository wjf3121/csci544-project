#!/usr/bin/env python3
import json
# from jsonrpc import ServerProxy, JsonRpc20, TransportTcpIp
import jsonrpclib
from pprint import pprint


class StanfordNLP:

    def __init__(self, port_number=8080):
        self.server = jsonrpclib.Server("http://localhost:%d" % port_number)

    def parse(self, text):
        return json.loads(self.server.parse(text))

if __name__ == '__main__':
    nlp = StanfordNLP()
    result = nlp.parse(
        '''Visiting Postino Wine Cafe makes me feel like I am in another city... an urban, metro, pedestrian city such as San Francisco. Its casual, yet sophisticated and quite enjoyable. I recently visited Postino with two friends and had the Bruschetta -- we selected the Fresh Mozzarella with Tomato and Basil, the Smoked Salmon and the Roasted Peppers with Goat Cheese. Thumbs up on all of these yummy appetizers! The clientele is a hip, diverse crowd reflective of its gentrified, charming neighborhood of mid-century ranch homes, and yet just a hop from the Biltmore and Arcadia neighborhoods. Ah... Phoenix is growing up! Wish I knew more about wines to tell you more about that. It appeared to be a nice selection and there were bottles you can buy to take home as well. Parking is a challenge, however I will definitely be back.

Update: Postino's is one of many restaurants/bars in AZ that permits concealed weapons without permits and is noted on the Arizona State Rifle and Pistol Association in their CCW Cafe page''')

    pprint(result)

    print(result['sentences'][0]['parsetree'])

    from nltk.tree import Tree
    tree = Tree.fromstring(result['sentences'][0]['parsetree'])
    pprint(tree)
