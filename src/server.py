#!/usr/bin/env python3
from flask import Flask, render_template, request
from yelper import Yelper
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def index():
	if request.method == 'GET':
		return render_template('base.html', result=None, question=True)
	if request.method == 'POST':
		try:
			question = request.form['question']
			result = Yelper().get_res_sum(question)
			return render_template('base.html', restautants=result, question=True)
		except:
			return render_template('base.html', restautants=[], question=True)


@app.route('/all')
def all():
	restautants = Yelper().get_all_sum()
	return render_template('base.html', question=False, restautants=restautants)


if __name__ == '__main__':
	app.run()
