#!/usr/bin/env python3
from collections import defaultdict
from utility import progressbar, word_filters
import math
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.cluster import KMeans
from gensim.models import Word2Vec
import pickle
from setting import Setting


class NBClassifier():

    def __init__(self, token_count, glbal_stats, expanded_dict=None):
        self.read_model(token_count, glbal_stats)
        self.expanded_dict = expanded_dict

    def read_model(self, token_count, glbal_stats):
        self.label_word_dict = token_count
        self.voc_size = token_count.find().count()
        self.label_count, self.label_words_sum = {}, {}
        for doc in glbal_stats.find():
            self.label_count[doc['_id']] = doc['doc']
            self.label_words_sum[doc['_id']] = doc['total']

    def get_word_count(self, label, word, default=0):
        word_item = self.label_word_dict.find_one({'_id': word})
        return default if word_item is None else word_item['value'][label]

    def get_words_count(self, words):
        word_items = self.label_word_dict.find({'_id': {"$in": words}})
        word_count = defaultdict(lambda: defaultdict(int))
        for word_item in word_items:
            for label in word_item['value']:
                word_count[label][word_item['_id']] = word_item['value'][label]
                if self.expanded_dict is not None and word_item['_id'] in self.expanded_dict[label]:
                    word_count[label][
                        word_item['_id']] += self.expanded_dict[label][word_item['_id']]
        return word_count

    def build_feature(self, words):
        ns = [2, 3]
        feature = []
        for n in ns:
            for i in range(len(words) - n + 1):
                feature.append(
                    '_'.join([w['word'].lower() for w in words[i:i + n]]))
        return feature

    def classify(self, sent):
        def p_w_c(n, N, k):
            return math.log((n + 1) / (N + (k if n == 0 else k + 1)))
        words = sent['tokenized']
        feature = self.build_feature(words)
        words_count = self.get_words_count(feature)
        p_label, label_sum = {}, sum(self.label_count.values())
        for label in self.label_count:
            p_label[label] = self.label_count[label] / label_sum
        result = []
        max_p, max_label = float('-inf'), None
        for label in self.label_count:
            N, k = self.label_words_sum[label], self.voc_size
            p_words_label = [p_w_c(words_count[label][f],
                                   N, k) for f in feature]
            p_feature_label = sum(p_words_label) + math.log(p_label[label])
            if p_feature_label > max_p:
                max_p, max_label = p_feature_label, label
            result.append((label, p_feature_label))
        return result, max_label


class WordVecClassifier():

    def __init__(self, w2v_path, dimension=100, expanded_sents=[]):
        self.dimension = dimension
        # self.wordvec = Word2Vec.load_word2vec_format(w2v_path, binary=True)
        self.wordvec = Word2Vec.load(w2v_path)
        self.expanded_sents = expanded_sents

    def read_model(self, path):
        with open(path, "rb") as f:
            self.forest = pickle.load(f)

    def makeFeatureVec(self, sents, word_filter=lambda x: True):
        featureVec = np.zeros((self.dimension,), dtype="float32")

        nwords = 0

        index2word_set = set(self.wordvec.index2word)

        for sent in sents:
            for word in sent['tokenized']:
                if word['word'].lower() in index2word_set and word_filter(word['word']):
                    nwords = nwords + 1
                    featureVec = np.add(
                        featureVec, self.wordvec[word['word'].lower()])
        if nwords > 0:
            featureVec = np.divide(featureVec, nwords)
        return featureVec

    def getAvgFeatureVecs(self, reviews, word_filter=lambda x: True):
        counter = 0

        labelVec = []

        reviewFeatureVecs = np.zeros(
            (reviews.count(), self.dimension), dtype="float32")
        for review in progressbar(reviews, 'reviews', size=30, count=reviews.count()):
            reviewFeatureVecs[counter] = self.makeFeatureVec(review['sents'])

            labelVec.append('pos' if review['stars'] > 3 else 'neg')

            counter += 1
        '''
		for sent in progressbar(self.expanded_sents):
			reviewFeatureVecs[counter] = self.makeFeatureVec(
				[sent[0]], word_filter)

			labelVec.append(sent[1])

			counter += 1
		'''
        print(len(reviewFeatureVecs), len(labelVec))
        return reviewFeatureVecs, labelVec

    def train(self, train_vecs, train_labels):
        forest = RandomForestClassifier(n_estimators=100)

        forest = forest.fit(train_vecs, train_labels)
        return forest

    def save_model(self, forest, path):
        with open(path, 'wb') as handle:
            pickle.dump(forest, handle)

    def classify(self, sent, word_filter=lambda x: True):
        feature = self.makeFeatureVec([sent], word_filter)
        return self.forest.predict(feature)


class WordVecClusterClassifier():

    def __init__(self, w2v_path, dimension=100, expanded_sents=[]):
        self.dimension = dimension
        # self.wordvec = Word2Vec.load_word2vec_format(w2v_path, binary=True)
        self.wordvec = Word2Vec.load(w2v_path)
        self.expanded_sents = expanded_sents

    def create_bag_of_centroids(self, sents, word_centroid_map, num_centroids, word_filter=lambda x: True):
        bag_of_centroids = np.zeros(num_centroids, dtype="float32")
        for sent in sents:
            for word in sent['tokenized']:
                if word['word'].lower() in word_centroid_map and word_filter(word['word']):
                    index = word_centroid_map[word['word'].lower()]
                    bag_of_centroids[index] += 1
        return bag_of_centroids

    def read_cluster(self, path):
        with open(path, "rb") as f:
            self.word_centroid_map = pickle.load(f)

    def read_model(self, path):
        with open(path, "rb") as f:
            self.forest = pickle.load(f)

    def train_cluster(self, path, word_filter=lambda x: True):
        word_vectors = self.wordvec.syn0
        num_clusters = int(word_vectors.shape[0] / 5)
        kmeans_clustering = KMeans(n_clusters=num_clusters)
        idx = kmeans_clustering.fit_predict(word_vectors)
        word_centroid_map = dict(zip(self.wordvec.index2word, idx))
        with open(path, 'wb') as handle:
            pickle.dump(word_centroid_map, handle)

    def get_feature_labels(self, reviews):
        counter = 0
        labelVec = []
        word_vectors = self.wordvec.syn0
        num_clusters = int(word_vectors.shape[0] / 5)
        num_centroids = max(list(self.word_centroid_map.values())) + 1
        train_centroids = np.zeros(
            (reviews.count(), num_clusters), dtype="float32")
        for review in progressbar(reviews, 'reviews', size=30, count=reviews.count()):
            train_centroids[counter] = self.create_bag_of_centroids(
                review['sents'], self.word_centroid_map, num_centroids)

            labelVec.append('pos' if review['stars'] > 3 else 'neg')

            counter += 1
        print(len(train_centroids), len(labelVec))
        return train_centroids, labelVec

    def train(self, train_vecs, train_labels):
        forest = RandomForestClassifier(n_estimators=100)

        forest = forest.fit(train_vecs, train_labels)
        return forest

    def save_model(self, forest, path):
        with open(path, 'wb') as handle:
            pickle.dump(forest, handle)

    def classify(self, sent, word_filter=lambda x: True):
        num_centroids = max(list(self.word_centroid_map.values())) + 1
        feature = self.create_bag_of_centroids(
            [sent], self.word_centroid_map, num_centroids, word_filter)
        return self.forest.predict(feature)


def adapt_with_diversity(db):
    expanded_dict_old = None
    # with open('../model/expand_dict.bin', "rb") as f:
    #	expanded_dict_old = pickle.load(f)
    nbc = NBClassifier(
        db.ngram_count_all, db.global_stat_all, expanded_dict_old)
    wvc = WordVecClassifier('../model/reviews_vec_all_100.mm', 100)
    wvc.read_model('../model/forest_small.mm')
    wvcc = WordVecClusterClassifier(
        '../model/reviews_vec_all_100.mm', 100)
    wvcc.read_cluster('../model/cluster.mm')
    wvcc.read_model('../model/forest_cluster.mm')
    tagged_reviews = db.tagged_restaurant_review.find()
    count = 0
    count_all = 0
    #with open('../model/expand_dict.bin', "rb") as f:
    #    expand_dict = pickle.load(f)
    # expand_sents = []
    expand_dict = defaultdict(dict)
    for review in progressbar(tagged_reviews, prefix='reviews:', count=tagged_reviews.count()):
        for sent in review['sents']:
            count_all += 1
            result_wv = wvc.classify(sent)
            result_nb = nbc.classify(sent)[1]
            result_wvc = wvcc.classify(sent)
            if result_wv == result_nb and result_wvc == result_nb:
                count += 1

                feature = nbc.build_feature(sent['tokenized'])
                for f in feature:
                    if f not in expand_dict[result_nb]:
                        expand_dict[result_nb][f] = 0
                    expand_dict[result_nb][f] += 1
                # expand_sents.append((sent, result_wv))
    with open('../model/expand_dict_cluster.bin', 'wb') as handle:
        pickle.dump(expand_dict, handle)
    # with open('../model/expand_sents.bin', 'wb') as handle:
    #    pickle.dump(expand_sents, handle)
    print(count)
    print(count / count_all)

if __name__ == '__main__':
    from pymongo import MongoClient
    db = MongoClient().yelp
    word_filter = word_filters[Setting.LDA_WORD_FILTER]
    adapt_with_diversity(db)
    expanded_sents = []
    '''
	nbc = NBClassifier(db.ngram_count, db.global_stat)
	# test_review_id = 'ZYjhOLd7pH6NfCEIMugdHg'
	# test_review = db.tagged_restaurant_review.find_one(
	#    {'review_id': test_review_id})
	# for sent in test_review['sents']:
	#    print(nbc.classify(sent['tokenized']))
	with open('../model/expand_sents.bin', "rb") as f:
			expanded_sents = pickle.load(f)
	'''
    '''
	wvc = WordVecClassifier(
		'../model/reviews_vec_all_100.mm', 100, expanded_sents)
	train_vecs, train_labels = wvc.getAvgFeatureVecs(
		db.tagged_restaurant_review.find().batch_size(30), word_filter)
	del expanded_sents[:]
	forest = wvc.train(train_vecs, train_labels)
	wvc.save_model(forest, '../model/forest_new.mm')
	'''
    '''
	wvcc = WordVecClusterClassifier(
		'../model/reviews_vec_all_100.mm', 100, expanded_sents)
	# wvcc.train_cluster('../model/cluster.mm', word_filter)
	wvcc.read_cluster('../model/cluster.mm')
	train_vecs, train_labels = wvcc.get_feature_labels(
		db.tagged_restaurant_review.find().batch_size(30))
	forest = wvcc.train(train_vecs, train_labels)
	wvcc.save_model(forest, '../model/forest_cluster.mm')
	'''
