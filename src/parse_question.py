from nltk.tag.stanford import NERTagger
import nltk
import json
from pymongo import MongoClient
import copy
from pprint import pprint
import lda
from utility import progressbar, word_filters, word_processors
from setting import Setting
from gensim.models.ldamodel import LdaModel
from gensim import corpora

# from jsonrpc import ServerProxy, JsonRpc20, TransportTcpIp
import jsonrpclib
from stanford_parser import StanfordNLP


def find_other_feature(dependency, res='NONE'):
    result = {}
    result['res'] = res
    result['poss'] = 'NONE'
    result['modpos'] = 'NONE'
    result['modres'] = 'NONE'
    result['neg'] = 'NONE'
    for d in dependency:
        if d[0] == 'poss' and d[2] == res:
            result['poss'] = d[1]
            for dd in dependency:
                if dd[0] == 'nn' and dd[1] == d[1]:
                    result['poss'] = dd[2]
                    result['modpos'] = dd[1]
        elif d[0] == 'dep' and d[1] == res:
            result['poss'] = d[2]
        elif (d[0] == 'prep_of' or d[0] == 'prep_in' or d[0] == 'prep_at') and d[2] == res:
            result['poss'] = d[1]

    for d in dependency:
        if d[0] == 'nsubj':
            if result['poss'] == 'NONE':
                if d[2] == res:
                    for dd in dependency:
                        if dd[0] == 'dobj' and dd[1] == d[1]:
                            result['poss'] = dd[2]
                            break
                    if result['poss'] == 'NONE':
                        # result['nsubj']=d[2]
                        result['modres'] = d[1]
            else:
                if d[2] == result['poss']:
                    # result['nsubj']=d[2]
                    result['modpos'] = d[1]
                if d[2] == res:
                    # result['nsubj']=result['poss']
                    result['modres'] = d[1]
    for d in dependency:
        if d[0] == 'amod':
            if d[1] == res:
                result['modres'] = d[2]
            if result['poss'] != 'NONE' and d[1] == result['poss'] and result['modpos'] == 'NONE':
                result['modpos'] = d[2]
    for d in dependency:
        if d[0] == 'neg':
            if d[1] == result['modres'] or d[1] == result['modpos']:
                result['neg'] = d[1]
    for d in dependency:
        if d[0] == 'prep_for':
            if d[2] == result['res'] or d[2] == result['poss']:
                result['feat'] = d[1]
    for d in dependency:
        tmpstr = ""
        if d[0][0:4] == "conj":
            if d[1] == result['poss']:
                tmpstr = tmpstr + ' ' + d[2]
            if d[2] == result['poss']:
                tmpstr = tmpstr + ' ' + d[1]
        result['poss'] = result['poss'] + tmpstr

    return result


def lookup_answer(single_query, ques, db):
    # print(single_query)
    dicfile = open('../model/topic_dict_q.txt', 'r')
    dic = {}
    count = 0
    for line in dicfile:
        dic[count] = line.strip()
        count += 1
    aspect = ['food', 'service', 'env', 'value']

    word_filter = word_filters[Setting.LDA_WORD_FILTER]
    word_processor = word_processors[Setting.LDA_WORD_PROCESSOR]
    lda_model = lda.load_lda_model(Setting.LDA_MODEL_PATH)
    dictionary = lda.load_dict(Setting.LDA_DICT_PATH)
    tmp_ques = []
    for w in ques.split():
        # splitq=single_query[w].split()
        # for ww in splitq:
        #     if ww!='NONE':
        tmp_ques.append({'word': w, 'pos': ''})

    result_topics = lda.predict(
        lda_model, dictionary, tmp_ques, word_filter, word_processor)
    origin_topic = single_query['poss'].split()
    new_topic = []
    for t in origin_topic:
        if t in aspect:
            new_topic.append(t)

    for r in result_topics:
        if r[1] > 0.2:
            if dic[r[0]] not in new_topic and dic[r[0]] in aspect:
                new_topic.append(dic[r[0]])

    if len(new_topic) == 0:
        single_query['poss'] = 'NONE'
    else:
        single_query['poss'] = ' '.join(new_topic)
    # print(new_topic)
    if single_query['res'] != 'NONE':
        # print('here')
        finalresult = {}
        ori_name = single_query['res'].replace('_', ' ')
        biz_id = db.restaurant.find({'name': str(ori_name)})[0]['business_id']
        # print(biz_id)
        result = db.summerized_result.find({'business_id': biz_id})[0]
        if single_query['poss'] != 'NONE':
            for t in new_topic:
                a = t  # change to w2v later
                # for pol in result[a]:
                #     for s in result[a][pol]:
                #         print(s)
                finalresult[a] = result[a]
        else:
            finalresult = result
        finalresult['name'] = ori_name
        return finalresult


def extract_sum(db, ques):
    st = NERTagger('../yelp_lib/stanford-ner-2015-04-20/ner-model.res.gz',
                   '../yelp_lib/stanford-ner-2015-04-20/stanford-ner.jar')
    nlp = StanfordNLP()
    # db = yelper.db
    while True:
        # s="Is Border_Grill's service good?"Can you give me some
        # recommendation for restaurant with good service
        s = ques
        #s="Can you give me some recommendation for food of Border_Grill?"
        #s="Can't miss stop for the not very nice Fish Sandwich in Pittsburgh."
        if s == "quit":
            break
        w = nltk.word_tokenize(s)
        # print(w)
        res_tag = st.tag(w)
        res = []
        ori_res = []
        cat_res = []
        #testtag=[[('Is', 'O'), ('The', 'RES'), ('Buffet', 'RES'), (',', 'O'), ('Bachi', 'RES'), ('Burger', 'RES'), ("'s", 'O'), ('food', 'O'), ('good', 'O'), ('?', 'O')]]
        for sent_tag in [res_tag]:
            tmp_res = []
            for t in sent_tag:
                if t[1] == 'RES':
                    tmp_res.append(t[0])
                if len(tmp_res) > 0 and t[1] == 'O':
                    res.append(tmp_res)
                    tmp_res = []
            if len(tmp_res) > 0:
                res.append(tmp_res)
            # print(res)
        query = []
        sent_with_catname = copy.deepcopy(s)
        for name in res:
            ori_name = ' '.join(name)
            cat_name = '_'.join(name)
            ori_name = ori_name.replace(" \'s", "\'s").replace(" .", ".")
            cat_name = cat_name.replace("_\'s", "\'s").replace("_.", ".")
            ori_res.append(ori_name)
            cat_res.append(cat_name)
            # print(ori_name)
            # print(cat_name)
            res_count = db.restaurant.find({'name': ori_name}).count()
            if res_count == 0:
                break
            sent_with_catname = sent_with_catname.replace(ori_name, cat_name)
            # print(sent_with_catname)

        parse_result = nlp.parse(sent_with_catname)
        # pprint(parse_result)
        single_query = find_other_feature(
            parse_result['sentences'][0]['dependencies'], cat_name)
        # print(single_query)
        return lookup_answer(single_query, ques, db)


def main():
    st = NERTagger('../../yelp_lib/stanford-ner-2015-04-20/ner-model.res.gz',
                   '../../yelp_lib/stanford-ner-2015-04-20/stanford-ner.jar')
    nlp = StanfordNLP()
    db = MongoClient().yelp
    while True:
        # s="Is Border_Grill's service good?"Can you give me some
        # recommendation for restaurant with good service
        s = input("Ask your question here:")
        #s="Can you give me some recommendation for food of Border_Grill?"
        #s="Can't miss stop for the not very nice Fish Sandwich in Pittsburgh."
        if s == "quit":
            break
        w = nltk.word_tokenize(s)
        # print(w)
        res_tag = st.tag(w)
        res = []
        ori_res = []
        cat_res = []
        #testtag=[[('Is', 'O'), ('The', 'RES'), ('Buffet', 'RES'), (',', 'O'), ('Bachi', 'RES'), ('Burger', 'RES'), ("'s", 'O'), ('food', 'O'), ('good', 'O'), ('?', 'O')]]
        for sent_tag in res_tag:
            tmp_res = []
            for t in sent_tag:
                if t[1] == 'RES':
                    tmp_res.append(t[0])
                if len(tmp_res) > 0 and t[1] == 'O':
                    res.append(tmp_res)
                    tmp_res = []
            if len(tmp_res) > 0:
                res.append(tmp_res)
        query = []
        sent_with_catname = copy.deepcopy(s)
        for name in res:
            ori_name = ' '.join(name)
            cat_name = '_'.join(name)
            ori_name = ori_name.replace(" \'s", "\'s").replace(" .", ".")
            cat_name = cat_name.replace("_\'s", "\'s").replace("_.", ".")
            ori_res.append(ori_name)
            cat_res.append(cat_name)
            res_count = db.restaurant.find({'name': ori_name}).count()
            if res_count == 0:
                break
            sent_with_catname = sent_with_catname.replace(ori_name, cat_name)

        parse_result = nlp.parse(sent_with_catname)
        single_query = find_other_feature(
            parse_result['sentences'][0]['dependencies'], cat_name)
        print(single_query)
        print(lookup_answer(single_query, s, db))

        # for name in cat_res:
        #   single_query=find_other_feature(parse_result['sentences'][0]['dependencies'],name)
        #   print(single_query)

        # for r in res_entity:
        # print(r)
        # print(res_entity)
        # print(db.restaurant.find({'name': 'hahaha'}).count())

        # break

if __name__ == '__main__':
    main()
# def find_feature2(dependency,res='NONE'):
#   result=[res,'NONE','NONE','NONE','NONE']
#   result['poss']='NONE'
#   for d in dependency:
#       if d[0]=='poss' and d[2]==res:
#           result[1]=d[1]
#       if d[0]=='dep' and d[1]==res:
#           result[1]=d[2]
#   for d in dependency:
#       if d[0]=='nsubj' :
#           if result['poss']=='NONE' and d[2]==res:
#               result['nsubj']=d[2]
#               result['modres']=d[1]
#           else:
#               if d[2]==result['poss']:
#                   result['nsubj']=d[2]
#                   result['modres']=d[1]
#               if d[2]==res:
#                   result['nsubj']=result['poss']
#                   result['modres']=d[1]
#       if d[0]=='amod':
#           if d[2]==res:
#               result['modnp']=d[1]
#           if result['poss']!='NONE' and d[2]==result['poss']:
#               result['modpos']=d[1]
#   for d in dependency:
#       if d[0]=='neg':
#           if ('modres' in result and d[1]==result['modres']) or ('modpos' in result and d[1]==result['modpos']):
#               result['neg']=d[1]
#   return result

# def find_other_feature(NP,dependency):
#   result={}
#   result['NP']=NP
#   result['poss']='NONE'
#   for d in dependency:
#       if d[0]=='poss' and d[2]==NP:
#           result['poss']=d[1]
#   for d in dependency:
#       if d[0]=='nsubj' :
#           if result['poss']=='NONE' and d[2]==NP:
#               result['nsubj']=d[2]
#               result['modres']=d[1]
#           else:
#               if d[2]==result['poss']:
#                   result['nsubj']=d[2]
#                   result['modres']=d[1]
#       if d[0]=='amod':
#           if d[2]==NP:
#               result['modnp']=d[1]
#           if result['poss']!='NONE' and d[2]==result['poss']:
#               result['modpos']=d[1]
#   return result
