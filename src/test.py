#!/usr/bin/env python3
from sentiment import NBClassifier, WordVecClassifier, WordVecClusterClassifier
from utility import read_topic_dict
import lda
from setting import Setting
from utility import word_filters, word_processors, select_topics
import pickle


def eval_sentiment():
    result = []
    from pymongo import MongoClient
    db = MongoClient().yelp
    with open('../model/expand_dict_cluster.bin', "rb") as f:
            expanded_dict = pickle.load(f)
    nbc = NBClassifier(db.ngram_count_all, db.global_stat_all, expanded_dict)
    wvc = WordVecClassifier('../model/reviews_vec_all_100.mm', 100)
    wvc.read_model('../model/forest_new.mm')
    wvcc = WordVecClusterClassifier(
        '../model/reviews_vec_all_100.mm', 100)
    wvcc.read_cluster('../model/cluster.mm')
    wvcc.read_model('../model/forest_cluster.mm')

    tagged_reviews = db.tagged_restaurant_review.find().sort(
        [('review_id', 1)]
    ).limit(100)
    for review in tagged_reviews:
        for sent in review['sents']:
            result.append(nbc.classify(sent)[1])
            #result.append('pos' if review['stars'] > 3 else 'neg')
    correct = []
    with open('../data/test_result_sentiment.txt', 'r') as f:
        for line in f:
            correct.append(line.strip().lower())
    labels = ['pos', 'neg']
    for l in labels:
        count, p, r = 0, 0, 0
        for i, j in zip(result, correct):
            if j != 'no':
                if i == l:
                    p += 1
                if j == l:
                    r += 1
                if i == j and i == l:
                    count += 1
        print(count / p, count / r, 2 * (count / p * count / r) / (count / p + count / r))


def read_topic_result():
    results = []
    t_dict = {'FO': 'food', 'SE': 'service', 'VA': 'value', 'DE': 'env'}
    with open('../data/test_result_topic.txt', 'r') as f:
        for line in f:
            result = set()
            for item in line.split():
                if item in t_dict:
                    result.add(t_dict[item])
            results.append(result)
    return results


def eval_topic():
    word_filter = word_filters[Setting.LDA_WORD_FILTER]
    word_processor = word_processors[Setting.LDA_WORD_PROCESSOR]
    topic_dict = read_topic_dict(Setting.TOPIC_DICT_PATH)
    lda_model = lda.load_lda_model(Setting.LDA_MODEL_PATH)
    dictionary = lda.load_dict(Setting.LDA_DICT_PATH)
    from pymongo import MongoClient
    db = MongoClient().yelp
    tagged_reviews = db.tagged_restaurant_review.find().sort(
        [('review_id', 1)]
    ).limit(100)
    count = 0
    results = []
    for review in tagged_reviews:
        for sent in review['sents']:
            topics = lda.predict(
                lda_model, dictionary, sent[
                    'tokenized'], word_filter, word_processor
            )
            results.append(select_topics(topics, topic_dict))
    topics = ['food', 'service', 'value', 'env']
    correct = read_topic_result()
    '''
    for i, r in enumerate(results):
        print(i+1)
        print(r)
    '''
    for topic in topics:
        count, p, r = 0, 0, 0
        for i, j in zip(results, correct):
            if len(j) > 0:
                if topic in i:
                    p += 1
                if topic in j:
                    r += 1
                if topic in i and topic in j:
                    count += 1
        print(count / p, count / r, 2 * (count / p * count / r) / (count / p + count / r))

def eval_sentiscore():
    result=[]
    correct=[]
    with open('senti_standard','r') as standard:
        for line in standard:
            correct.append(line.strip())
    with open ('sentiscore_result2','r') as output:
        for line in output:
            result.append(line.strip())
    labels = ['POS', 'NEG']
    for l in labels:
        count, p, r = 0, 0, 0
        for i, j in zip(result, correct):
            if j != 'no':
                if i == l:
                    p += 1
                if j == l:
                    r += 1
                if i == j and i == l:
                    count += 1
        print(count / p, count / r, 2 * (count / p * count / r) / (count / p + count / r))

if __name__ == '__main__':
    eval_sentiment()
