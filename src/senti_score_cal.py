from nltk.tag.stanford import NERTagger
import nltk
import json
from pymongo import MongoClient
import copy
from pprint import pprint
import jsonrpclib
from collections import defaultdict

if __name__ == "__main__"	:
    infile = open('finalphrases', 'r')
    inadj = json.load(infile)
    adjlist = {}
    for score in inadj:
        for l in inadj[score]:
            for word in l:
                adj = l[word]['adj'].lower()
                if adj not in adjlist:
                    adjlist[adj] = {}
                    adjlist[adj][score] = 1
                else:
                    if score in adjlist[adj]:
                        adjlist[adj][score] += 1
                    else:
                        adjlist[adj][score] = 1
    print(adjlist)
    adjscore = {}
    for a in adjlist:
        ct = [0, 0, 0, 0, 0]
        ch = 0
        m = 0
        for i in range(1, 6):
            if (str(i) + '.0') in adjlist[a]:
                ct[i - 1] += 1
        for i in range(5):
            if ct[i] > 0:
                ch += (i + 1) / ct[i]
                m += 1 / ct[i]
        if m != 0:
            adjscore[a] = ch / m
    print(adjscore)
