#!/usr/bin/env python3
from setting import Setting
from pymongo import MongoClient
from utility import pos_tag_text, extract_stop_words, word_filters, word_processors, select_topics, read_topic_dict, progressbar
from collections import defaultdict
import argparse
import threading
import lda
#import sentiment
from summarizer import KLSum
import parse_question


class Yelper():

    def __init__(self):
        self.db = MongoClient().yelp

    def get_reviews_by_restaurant(self, business_id):
        return self.db.restaurant_review.find({'business_id': business_id})

    def get_restaurant_by_id(self, business_id):
        return self.db.restaurant.find_one({'business_id': business_id})

    def get_restaurant(self, query={}):
        return self.db.restaurant.find(query)

    def get_reviews(self, query={}):
        return self.db.restaurant_review.find(query)

    def get_tagged_review(self, query={}):
        return self.db.tagged_restaurant_review.find(query)

    def process_review(self, review):
        sents = pos_tag_text(review["text"])
        return {
            'review_id': review['review_id'],
            'business_id': review['business_id'],
            'sents': sents,
            'stars': review['stars']
        }

    def get_frequent_words(self):
        words = extract_stop_words(
            self.db.token_count.find().sort('value', -1))
        for word in words:
            print(word)

    def process_reviews(self):
        def f(cursor, *args):
            reviews = []
            for r in cursor:
                reviews.append(r)
            for i, r in enumerate(reviews):
                self.db.tagged_restaurant_review.insert_one(
                    self.process_review(r))
        self.__parallel_process(self.db.restaurant_review, f)

    def generate_topic_model(self):
        word_filter = word_filters[Setting.LDA_WORD_FILTER]
        word_processor = word_processors[Setting.LDA_WORD_PROCESSOR]
        dictionary = lda.build_dict(
            self.db.tagged_restaurant_review.find(),
            word_filter, word_processor
        )
        corpus = lda.build_corpus(
            dictionary, self.db.tagged_restaurant_review.find(),
            word_filter, word_processor
        )
        lda_model = lda.train(corpus, dictionary, Setting.LDA_TOPIC_NUM)
        lda.save_dict(dictionary, Setting.LDA_DICT_PATH)
        lda.save_lda_model(lda_model, Setting.LDA_MODEL_PATH)

    def topic_classify(self, topic_dict):
        lda_model = lda.load_lda_model(Setting.LDA_MODEL_PATH)
        dictionary = lda.load_dict(Setting.LDA_DICT_PATH)
        word_filter = word_filters[Setting.LDA_WORD_FILTER]
        word_processor = word_processors[Setting.LDA_WORD_PROCESSOR]
        review_topic_dict = defaultdict(lambda: defaultdict(list))
        for review in progressbar(self.get_tagged_review(), 'Topic ', 30, self.get_tagged_review().count()):
            for sent in review['sents']:
                # TODO process PRONOUNS
                topics = lda.predict(
                    lda_model, dictionary, sent[
                        'tokenized'], word_filter, word_processor
                )
                selected_topics = select_topics(topics, topic_dict)
                for t in selected_topics:
                    review_topic_dict[review['business_id']][t].append(sent)
        return review_topic_dict

    def sentiment_classify(self, review_topic_dict):
        nb_classifier = sentiment.NBClassifier(
            self.db.ngram_count, self.db.global_stat)
        review_topic_sentiment_dict = defaultdict(
            lambda: defaultdict(lambda: defaultdict(list)))
        for restaurant in progressbar(review_topic_dict, 'Sentiment', 30, len(review_topic_dict)):
            for topic in review_topic_dict[restaurant]:
                for sent in review_topic_dict[restaurant][topic]:
                    label = nb_classifier.classify(sent)[1]
                    review_topic_sentiment_dict[restaurant][
                        topic][label].append(sent)
        return review_topic_sentiment_dict

    def summerize_reviews(self, review_topic_sentiment_dict):
        KLSummer = KLSum()
        summerize_dict = defaultdict(
            lambda: defaultdict(dict))
        for restaurant in progressbar(review_topic_sentiment_dict, 'Sum', 30, len(review_topic_sentiment_dict)):
            for topic in review_topic_sentiment_dict[restaurant]:
                for senti_label in review_topic_sentiment_dict[restaurant][topic]:
                    sum_sents = KLSummer.greedy_sum(
                        review_topic_sentiment_dict[restaurant][topic][senti_label])
                    summerize_dict[restaurant][topic][
                        senti_label] = [s[0] for s in sum_sents]
        return summerize_dict

    def __parallel_process(self, collection, process_cursor, *args):
        cursors = collection.parallel_scan(4)
        threads = [
            threading.Thread(
                target=process_cursor, args=(cursor,) + tuple(args) + (i,))
            for i, cursor in enumerate(cursors)
        ]
        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

    def generate_test_sents(self):
        tagged_reviews = self.get_tagged_review().sort(
            [('review_id', 1)]).limit(100)
        count = 0
        for review in tagged_reviews:
            print('========' + str(review['stars']) + '========')
            for sent in review['sents']:
                print(count)
                print(sent['raw'])
                count += 1

    def save_result(self, summerize_dict):
        for restaurant in progressbar(summerize_dict, 'Save', 30, len(summerize_dict)):
            doc = {}
            doc['business_id'] = restaurant
            for topic in summerize_dict[restaurant]:
                doc[topic] = {}
                for label in summerize_dict[restaurant][topic]:
                    doc[topic][label] = summerize_dict[
                        restaurant][topic][label]
            print(doc)
            self.db.summerized_result.insert_one(doc)

    def get_all_sum(self):
        result = []
        summers = self.db.summerized_result.find()
        for restaurant in summers:
            r = self.get_restaurant_by_id(restaurant['business_id'])
            restaurant['name'] = r['name']
            result.append(restaurant)
        return result

    def pretty_print_sum(self):
        topics = ['food', 'service', 'value', 'env']
        labels = ['pos', 'neg']
        summers = y.db.summerized_result.find()
        for i, restaurant in enumerate(summers):
            print('#restaurant' + str(i))
            for topic in topics:
                print('##' + topic)
                for label in labels:
                    print('###' + label)
                    for sent in restaurant[topic][label]:
                        print('++++++++++++++')
                        print()
                        print(sent)
                        print()

    def get_res_sum(self, ques):
        result = []
        result.append(parse_question.extract_sum(self.db, ques))

        return result

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode',
                        type=int,
                        choices=[0, 1, 2, 3, 4, 5, 6, 7],
                        default=0,
                        help='Select mode')
    args = parser.parse_args()
    y = Yelper()
    if args.mode == 1:
        y.process_reviews()
    if args.mode == 2:
        y.get_frequent_words()
    if args.mode == 3:
        y.generate_topic_model()
    if args.mode == 4:
        y.generate_test_sents()
    if args.mode == 5:
        topic_dict = read_topic_dict(Setting.TOPIC_DICT_PATH)
        topic_result = y.topic_classify(topic_dict)
        sentiment_result = y.sentiment_classify(topic_result)
        summerize_result = y.summerize_reviews(sentiment_result)
        y.save_result(summerize_result)
    if args.mode == 6:
        print(y.get_all_sum())
    if args.mode == 7:
        print(y.get_res_sum(''))
