#!/usr/bin/env python3
from pymongo import MongoClient
from stanford_parser import StanfordNLP
import inspect
from utility import progressbar
import argparse
import threading
from collections import defaultdict
from nltk.tokenize import sent_tokenize, word_tokenize
import nltk
import re
from pprint import pprint
import json
import operator

# extract_tips from reviews of one business


def extract_tips(self, c):
    NN = defaultdict(int)
    dependency = defaultdict(list)
    reviews = c.find()  # change to related to business id later
    count = 0
    parse_t = StanfordNLP()
    for r in reviews:
        print(count)
        count += 1
        # if count==10:
        # 	break
        t = r["text"]
        # print(t)
        sentences = nltk.sent_tokenize(t)
        for sent in sentences:
            print(sent)
            tokens = nltk.word_tokenize(sent)
            tags = nltk.pos_tag(tokens)
            for tag in tags:
                if tag[1] == 'NN' or tag[1] == 'NNS':
                    result = parse_t.parse(t)
                    # pprint(result)
                    # token=nltk.word_tokenize(t)
                    # result2=nltk.pos_tag(token)
                    # pprint(result2)
                    for s in result['sentences']:
                        for w in s['words']:
                            lowerw = w[0].lower()
                            if w[1]['PartOfSpeech'] == 'NN' or w[1]['PartOfSpeech'] == 'NNS':
                                if lowerw in NN:
                                    NN[lowerw] += 1
                                else:
                                    NN[lowerw] = 1
                                for f in s['dependencies']:
                                    if lowerw == f[1] or lowerw == f[2]:
                                        dependency[lowerw].append(f)
                    break

    # NN=list(set(NN))

    print(NN)
    print(dependency)
    # extract useful from dependency: nsubj, dep, amod


def find_NNs(self, collections):
    NN = defaultdict(int)
    reviews = collections.find()
    count = 0
    for r in reviews:
        print(count)
        # if count==10:
        # 	break
        t = r["text"]
        sentences = nltk.sent_tokenize(t)
        for sent in sentences:
            tokens = nltk.word_tokenize(sent)
            tags = nltk.pos_tag(tokens)
            for tag in tags:
                if tag[1] == 'NN' or tag[1] == 'NNS':
                    lowerw = tag[0].lower()
                    if lowerw in NN:
                        NN[lowerw] += 1
                    else:
                        NN[lowerw] = 1
        count += 1
    print(NN)

    sorted_NN = sorted(
        NN.items(), key=operator.itemgetter(1), reverse=True)
    #NN = sorted(NN.items(), key=NN.get, reverse=True)
    print(sorted_NN)
    #print( NN )
    with open("NN.json", 'w') as modelf:
        json.dump(sorted_NN, modelf, indent=4)


def find_NN_bybus(self, collections):
    business_small = collections.find().sort([('business_id', 1)])
    NN = defaultdict(lambda: defaultdict(int))
    sorted_NN = {}
    with open("NNs.json", 'w') as modelf:
        for i, b in enumerate(business_small):
            print("here")
            review_small = self.db.review_small.find(
                {'business_id': b['business_id']})
            # print(review_small)
            for r in review_small:
                t = r["text"]
                sentences = nltk.sent_tokenize(t)
                for sent in sentences:
                    tokens = nltk.word_tokenize(sent)
                    tags = nltk.pos_tag(tokens)
                    for tag in tags:
                        if tag[1] == 'NN' or tag[1] == 'NNS':
                            lowerw = tag[0].lower()
                            if lowerw in NN[b['business_id']]:
                                NN[b['business_id']][lowerw] += 1
                            else:
                                NN[b['business_id']][lowerw] = 1
            sorted_NN[b['business_id']] = sorted(
                NN[b['business_id']].items(), key=operator.itemgetter(1), reverse=True)
        #NN = sorted(NN.items(), key=NN.get, reverse=True)
            #print( sorted_NN )
        json.dump(sorted_NN, modelf, indent=4)


def find_core_sen(self, collections):
    business_small = collections.find().sort([('business_id', 1)])
    dependency = defaultdict(lambda: defaultdict(list))
    idx_dependency = defaultdict(lambda: defaultdict(list))
    parse_t = StanfordNLP()
    with open("NNs.json", 'r') as modelf:
        sorted_NN = json.load(modelf)
        for i, b in enumerate(business_small):
            print("here")
            review_small = self.db.review_small.find(
                {'business_id': b['business_id']})
            # print(review_small)
            for r in review_small:
                t = r["text"]
                sentences = nltk.sent_tokenize(t)
                for sent in sentences:
                    tokens = nltk.word_tokenize(sent)
                    for n in range(int(len(sorted_NN[b['business_id']]) / 10)):
                        if sorted_NN[b['business_id']][n][1] == 1:
                            break
                        if sorted_NN[b['business_id']][n][0] in tokens:
                            result = parse_t.parse(sent)
                            for s in result['sentences']:
                                for w in s['words']:
                                    if w[1]['PartOfSpeech'] == 'NN' or w[1]['PartOfSpeech'] == 'NNS':
                                        dependency[b['business_id']][
                                            sent].append(s['dependencies'])
                                        idx_dependency[b['business_id']][
                                            sent].append(s['indexeddependencies'])
                                        break
                            break

        #NN = sorted(NN.items(), key=NN.get, reverse=True)
            #print( sorted_NN )
        dep = open("dependency.json", 'w')
        idx_dep = open("idx_dependency.json", 'w')
        json.dump(dependency, dep, indent=4)
        json.dump(idx_dependency, idx_dep, indent=4)
        dep.close()
        idx_dep.close()
