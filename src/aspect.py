#!/usr/bin/env python3
from gensim.models.ldamodel import LdaModel
from gensim import corpora
from utility import filt_word, progressbar, filt_word_alpha
from setting import Setting
import re
import gensim.models.word2vec  as w2v
from collections import defaultdict
from lda import load_dict, load_lda_model

if __name__ == '__main__':
	lda_model = load_lda_model(Setting.LDA_MODEL_PATH)
	dictionary = load_dict(Setting.LDA_DICT_PATH)
	model=w2v.Word2Vec.load('reviews_vec_1000')
	print(model.similarity('food','service'))
	# print('food' in model)
	aspectf=open('aspecttest','w')

	for i in range(0, lda_model.num_topics):
		words = re.findall(r'''[a-zA-Z]+''',lda_model.print_topic(i))
		coef=re.findall(r'''0\.[0-9]+''',lda_model.print_topic(i))
		# print(words)
		# print(coe)

		result=defaultdict(float)
		for j in range(len(words)):
			if words[j] in model:
				result['food']+=float(coef[j])*model.similarity(words[j],'food')
				result['service']+=float(coef[j])*model.similarity(words[j],'service')
				result['decor']+=float(coef[j])*model.similarity(words[j],'decor')
				result['value']+=float(coef[j])*model.similarity(words[j],'value')
		aspect=max(result,key=result.get)
		print(aspect+'	'+lda_model.print_topic(i))
		aspectf.write(aspect+'	'+lda_model.print_topic(i)+'\n')
		#print(result)


