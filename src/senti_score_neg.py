from nltk.tag.stanford import NERTagger
import nltk
import json
from pymongo import MongoClient
import copy
from pprint import pprint
import jsonrpclib
from collections import defaultdict
from stanford_parser import StanfordNLP


def find_phrase(parse_result, star):
    phrase = {}
    # result['res']=res
    # result['poss']='NONE'
    # result['modpos']='NONE'
    # result['modres']='NONE'
    # result['neg']='NONE'
    for w in parse_result['words']:
        if w[1]['PartOfSpeech'] == 'JJ':
            if w[0] not in phrase:
                wl = w[0].lower()
                phrase[wl] = {}
                phrase[wl][str(star)] = {}
                for d in parse_result['dependencies']:
                    if d[0] == 'advmod' and d[1].lower() == wl:
                        d2 = d[2]
                        if d2 in phrase[wl][str(star)]:
                            phrase[wl][str(star)][d2] += 1
                        else:
                            phrase[wl][str(star)][d2] = 1
                    if d[0] == 'neg' and d[1].lower() == wl:
                        if 'not' in phrase[wl][str(star)]:
                            phrase[wl][str(star)]['not'] += 1
                        else:
                            phrase[wl][str(star)]['not'] = 1

    return phrase


if __name__ == "__main__"	:
    #st = NERTagger('/Users/maijia/work/stanford-ner-2015-04-20/ner-model.res.gz','/Users/maijia/work/stanford-ner-2015-04-20/stanford-ner.jar')
    nlp = StanfordNLP()
    db = MongoClient().yelp
    tagged_reviews = db.restaurant_review_all.find(
        {'stars': 1}, no_cursor_timeout=True)
    finalphrases = defaultdict(list)
    count = 0
    jsfile = open('finalphrases_all_neg', 'w')
    for tr in tagged_reviews:
        print(count)
        # if count<289:
        # 	count+=1
        # 	continue
        star = int(tr['stars'])
        # s="Is Border_Grill's service good?"Can you give me some recommendation for restaurant with good service
        #s=input("Ask your question here:")
        #s="Can you give me some recommendation for food of Border_Grill?"
        sr = tr['text']
        #s="Can't miss stop for the not very nice Fish Sandwich in Pittsburgh."
        # sr=s['raw']
        sr = nltk.sent_tokenize(sr)
        # if s=="quit":
        # 	break
        #w= nltk.word_tokenize(s)
        # print(w)
        # res_tag=st.tag(w)
        #"I've seen a bunch of glowing reviews on good ol' Yelp and as I always do when I read the praise heaped on a particular place by my Yelp brethren and sistren, I thought "I'll be the judge of that!"
        for sub_sent in sr:
            print(sub_sent)
            try:
                parse_result = nlp.parse(sub_sent)
            except:
                break
            else:
                if 'sentences' in parse_result and len(parse_result['sentences']) > 0 and 'dependencies'in parse_result['sentences'][0]:
                    phrase = find_phrase(parse_result['sentences'][0], star)
                    if len(phrase) > 0:
                        print(phrase)
                        jsfile.write(str(phrase) + '\n')
                        # finalphrases[str(star)].append(phrase)
            # pprint(parse_result)
        # break
        # if count>1000:
        # 	tagged_reviews=[]
        # 	break
        count += 1
        if count == 20:
            jsfile.flush()
    tagged_reviews = []
    jsfile.close()
    # print(finalphrases)
    # json.dump(finalphrases,jsfile,indent=4)
