import gensim.models.word2vec as w2v
from pymongo import MongoClient
#from nltk.tokenize import sent_tokenize, word_tokenize
# infile=open("reviews.txt",'r')
# for line in infile:
# 	print(line)
# infile.seek(0)


def gen_tokenized_sent():
    sentences = []
    db = MongoClient().yelp
    tagged_words = db.tagged_restaurant_review.find()
    count = 0
    for tw in tagged_words:
        for s in tw['sents']:
            sent = []
            for w in s:
                sent.append(w['word'])
            sentences.append(sent)
        # count+=1
        # if count==10000:
        # 	break
    return sentences

if __name__ == '__main__':
    # print(sentences)
    sentences = gen_tokenized_sent()
    print(len(sentences))
    # print(sentences)
    model = w2v.Word2Vec(
        sentences, size=1000, window=5, min_count=5, workers=4)
    # model.build_vocab(sentences)
    # sentences=w2v.LineSentence("reviewscopy.txt")
    # model.train(sentences)
    model.save("reviews_vec_1000")
    print(model.similarity('food', 'service'))
    # infile.close()
