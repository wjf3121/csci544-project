var mapf = function() {
    function word_process(words){
        var reg = /[^0-9a-zA-Z]+/;
        new_words = [];
        for(var i = 0; i < words.length; i++){
            new_word = words[i].word.replace(reg, "");
            if(new_word !== "")
                new_words.push(words[i].word.toLowerCase());
        }
        return new_words;
    }
    var sents = this.sents;

    var ns = [2,3];

    if (sents === null) {
        return;
    }
    for (var i = 0; i < sents.length; i++) {
        new_sent = word_process(sents[i].tokenized);
        for(var k = 0; k < ns.length; k++){
            n = ns[k];
            for(var j = 0; j < (new_sent.length - n + 1); j++){
                if(this.stars > 4)
                    emit(new_sent.slice(j, j+n).join("_"), {pos:1, neg:0});
                if(this.stars < 3)
                    emit(new_sent.slice(j, j+n).join("_"), {pos:0, neg:1});
            }
        }
    }
};


var reducef = function(key, values) {
    var pos_c = 0, neg_c = 0;
    values.forEach(function(v) {
        pos_c += v.pos;
        neg_c += v.neg;
    });
    return {pos:pos_c, neg:neg_c};
};


db.tagged_restaurant_review.mapReduce(
                     mapf,
                     reducef,
                     { out: "ngram_count_15" }
                   );